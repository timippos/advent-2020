#!/bin/bash
case $1 in
	"" )
		echo "At least one of --all or day number expected."
		;;
	"-a"|"--all" )
		for i in {1..9}; do
			if [ -e "data/input0$i" ]; then
				cat data/input0$i | stack run -- advent-2020 --day$i $2
			fi
		done
		for i in {10..25}; do
			if [ -e "data/input$i" ]; then
				cat data/input$i | stack run -- advent-2020 --day$i $2
			fi
		done
		;;
	*)
		cat data/input$1 | stack run -- advent-2020 --day$1 $2
		;;
esac
