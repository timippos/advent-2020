-- Advent of code 2020

import qualified Advent2020.Days.Day01 as Day01
import qualified Advent2020.Days.Day02 as Day02
import qualified Advent2020.Days.Day03 as Day03
import qualified Advent2020.Days.Day04 as Day04
import qualified Advent2020.Days.Day05 as Day05
import qualified Advent2020.Days.Day06 as Day06
import qualified Advent2020.Days.Day07 as Day07
import qualified Advent2020.Days.Day08 as Day08
import qualified Advent2020.Days.Day09 as Day09
import qualified Advent2020.Days.Day10 as Day10
import qualified Advent2020.Days.Day11 as Day11
import qualified Advent2020.Days.Day12 as Day12
import qualified Advent2020.Days.Day13 as Day13
import qualified Advent2020.Days.Day14 as Day14
import qualified Advent2020.Days.Day15 as Day15
import qualified Advent2020.Days.Day16 as Day16
import qualified Advent2020.Days.Day17 as Day17
import qualified Advent2020.Days.Day18 as Day18
import qualified Advent2020.Days.Day19 as Day19
import qualified Advent2020.Days.Day20 as Day20
import qualified Advent2020.Days.Day21 as Day21
import qualified Advent2020.Days.Day22 as Day22
import qualified Advent2020.Days.Day23 as Day23
import qualified Advent2020.Days.Day24 as Day24
import qualified Advent2020.Days.Day25 as Day25

import Data.Maybe
import Safe
import Advent2020.IO

main :: IO ()
main = do
    (operation, day, input) <- adventIO 
    if operation == noopPrim then return ()
    else do
        if operation /= onlyPart2Prim then do
            putStr $ "day" ++ show day ++ "p1: "
            let p1 = [ return "unsupported day"
                     , show <$> Day01.part1 input, show <$> Day02.part1 input
                     , show <$> Day03.part1 input, show <$> Day04.part1 input
                     , show <$> Day05.part1 input, show <$> Day06.part1 input
                     , show <$> Day07.part1 input, show <$> Day08.part1 input
                     , show <$> Day09.part1 input, show <$> Day10.part1 input
                     , show <$> Day11.part1 input, show <$> Day12.part1 input
                     , show <$> Day13.part1 input, show <$> Day14.part1 input
                     , show <$> Day15.part1 input, show <$> Day16.part1 input
                     , show <$> Day17.part1 input, show <$> Day18.part1 input
                     , show <$> Day19.part1 input, show <$> Day20.part1 input
                     , show <$> Day21.part1 input, show <$> Day22.part1 input
                     , show <$> Day23.part1 input, show <$> Day24.part1 input
                     , show <$> Day25.part1 input
                     ] `atMay` day in do
                         result <- fromMaybe (return "unsupported day") p1
                         putStrLn result
        else return ()
        if operation /= onlyPart1Prim then do
            putStr $ "day" ++ show day ++ "p2: "
            let p2 = [ return "unsupported day"
                     , show <$> Day01.part2 input, show <$> Day02.part2 input
                     , show <$> Day03.part2 input, show <$> Day04.part2 input
                     , show <$> Day05.part2 input, show <$> Day06.part2 input
                     , show <$> Day07.part2 input, show <$> Day08.part2 input
                     , show <$> Day09.part2 input, show <$> Day10.part2 input
                     , show <$> Day11.part2 input, show <$> Day12.part2 input
                     , show <$> Day13.part2 input, show <$> Day14.part2 input
                     , show <$> Day15.part2 input, show <$> Day16.part2 input
                     , show <$> Day17.part2 input, show <$> Day18.part2 input
                     , show <$> Day19.part2 input, show <$> Day20.part2 input
                     , show <$> Day21.part2 input, show <$> Day22.part2 input
                     , show <$> Day23.part2 input, show <$> Day24.part2 input
                     , show <$> Day25.part2 input
                     ] `atMay` day in do
                         result <- fromMaybe (return "unsupported day") p2
                         putStrLn result
        else return ()
