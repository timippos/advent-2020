# Advent of Code 2020

Solutions to the Advent of Code 2020 (see [official
website](http://adventofcode.com/)) game, written in Haskell.

Usage
--

Install stack and build. Run the solver with

`cat data/inputXX | advent-2015 --dayXX [--part-one-only] [--part-two-only]`

**Note:** The Advent of Code website generates different input for each signed
in user; replace my input files with yours.
