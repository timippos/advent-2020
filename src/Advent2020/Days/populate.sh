for i in {2..9}; do
	cp Day01.hs Day0${i}.hs
	sed -i -- "s/Day01/Day0${i}/g" Day0${i}.hs
done
for i in {10..25}; do
	cp Day01.hs Day${i}.hs
	sed -i -- "s/Day01/Day${i}/g" Day${i}.hs
done
