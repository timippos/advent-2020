module Advent2020.Days.Day08 ( part1, part2 ) where

import Text.Parsec (Parsec)
import qualified Text.Parsec as P
import Data.Set (Set, member)
import qualified Data.Set as S
import Control.Lens
import Advent2020.Text.Parsec

-- Data structure

data Instruction = Acc Int | Jmp Int | Nop Int
    deriving (Eq, Show)
type Program = [Instruction]

data Machine = Machine
    { program :: Program
    , visited :: Set Int
    , instPtr :: Int
    , dataReg :: Int
    } deriving (Show)

-- Logic

initialize :: Program -> Machine
initialize program = Machine program S.empty 0 0

looping :: Machine -> Bool
looping (Machine _ visited instPtr _) = instPtr `member` visited

halted :: Machine -> Bool
halted (Machine program _ instPtr _) = instPtr >= length program

executeOneInst :: Machine -> Machine
executeOneInst (Machine program visited instPtr dataReg) =
    let curInst = program !! instPtr
     in case curInst of
          Acc i -> Machine program (S.insert instPtr visited) (instPtr+1) (dataReg+i)
          Jmp i -> Machine program (S.insert instPtr visited) (instPtr+i) dataReg
          Nop _ -> Machine program (S.insert instPtr visited) (instPtr+1) dataReg

-- Execute the machine until it is either looping or halted
executeTillEnd :: Machine -> Machine
executeTillEnd m | halted m || looping m = m
executeTillEnd m = executeTillEnd $ executeOneInst m

fixAttempts :: Program -> [Program]
fixAttempts program = map (\n -> fixProg program n) 
                    $ filter (\n -> not $ isAcc $ program !! n) [0..length program-1]
    where fixInst (Jmp i) = Nop i
          fixInst (Nop i) = Jmp i
          isAcc (Acc _) = True
          isAcc _       = False
          fixProg p n = p & element n .~ fixInst (p !! n)

-- Input parsing

instParser :: Parsec String () Instruction
instParser = do
    opcode <- P.count 3 P.letter
    P.choice $ map P.try [P.string " +", P.string " "]
    oparg <- P.many1 $ P.choice [P.digit, P.char '-']
    case opcode of
      "nop" -> return $ Nop (read oparg)
      "acc" -> return $ Acc (read oparg)
      "jmp" -> return $ Jmp (read oparg)

parseProgram :: String -> Program
parseProgram = map (unsafeParse instParser) . lines

part1 :: String -> IO Int
part2 :: String -> IO (Maybe Int)
part1 = return . dataReg . executeTillEnd . initialize . parseProgram 
part2 content = case filter halted $ map (executeTillEnd . initialize)
                                   $ fixAttempts $ parseProgram content of
                  [] -> return Nothing
                  (result:_) -> return (Just $ dataReg result)
