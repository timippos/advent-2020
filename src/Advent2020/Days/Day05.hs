module Advent2020.Days.Day05 ( part1, part2 ) where

import Data.List (sort)
import Text.Parsec (Parsec)
import qualified Text.Parsec as P
import Advent2020.Text.Parsec

-- Logic

binToDec :: Char -> Char -> String -> Int
binToDec zero one bin
  | null bin  = 0 
  | otherwise = lastBit + 2*(binToDec zero one butLast)
        where len = length bin
              butLast = take (len-1) bin
              lastBit = if bin !! (len-1) == zero then 0 else 1

calcID :: (Int, Int) -> Int
calcID (x, y) = 8*x+y

isFrontOrEnd :: (Int, Int) -> Bool
isFrontOrEnd (x, _) = x==0 || x==127

findGap :: [Int] -> Maybe Int
findGap xs = findGap' (sort xs)
    where findGap' sorted | length sorted < 2 = Nothing
          findGap' (a:b:rest)
            | a+2 == b  = Just (a+1)
            | otherwise = findGap' (b:rest)

-- Input parsing

seatParser :: Parsec String () (Int, Int)
seatParser = (,) <$> fmap (binToDec 'F' 'B') (P.count 7 $ P.oneOf "FB")
                 <*> fmap (binToDec 'L' 'R') (P.count 3 $ P.oneOf "RL")

part1 :: String -> IO Int
part2 :: String -> IO (Maybe Int)
part1 = return . maximum . map (calcID . unsafeParse seatParser) . lines
part2 = return . findGap
               . map calcID . filter (not . isFrontOrEnd)
               . map (unsafeParse seatParser) . lines
