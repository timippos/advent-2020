-- In which I give up and just use C
-- because Haskell is just too slow :(

{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE TemplateHaskell #-}

module Advent2020.Days.Day15 ( part1, part2 ) where

import Data.List.Split (splitOn)
import qualified Data.Vector.Storable as V
import qualified Language.C.Inline as C
import Foreign.C.Types

C.context (C.baseCtx <> C.vecCtx)
C.include "<stdio.h>"
C.include "<stdlib.h>"
C.include "<string.h>"

-- Logic

-- This assumes all numbers in input are unique
elfSequence :: Int -> [Int] -> IO CInt
elfSequence n ns =
    let cn = CInt (fromIntegral n)
        vns = V.fromList $ map (CInt . fromIntegral) ns
     in [C.block| int {
            int len = $vec-len:vns, n = $(int cn);
            int* tab = (int*) malloc (sizeof(int)*(len+n));
            int result = 0, last_turn;
            memset (tab, 0, sizeof(tab));

            for (int i=1; i<=len; ++i) {
                int saying = $vec-ptr:(int *vns)[i-1];
                tab[saying] = i;
            }

            for (int i=len+1; i<n; ++i) {
                last_turn = tab[result];
                tab[result] = i;
                result = i-(last_turn ? last_turn : i);
            }

            free (tab);
            return result;
        } |]

part1, part2 :: String -> IO CInt
part1 = elfSequence 2020 . map read . splitOn ","
part2 = elfSequence 30000000 . map read . splitOn ","
