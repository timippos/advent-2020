module Advent2020.Days.Day07 ( part1, part2 ) where

import Data.Maybe (catMaybes)
import Data.List (nub)
import Data.Map.Strict (Map, (!), keys)
import qualified Data.Map.Strict as M
import Text.Parsec (Parsec)
import qualified Text.Parsec as P
import Advent2020.Text.Parsec
import Advent2020.Data.Map.Lazy (makeMap)

-- Data structure

type Color = String
type Content = (Color, Int)
type Rule = (Color, [Content])
type RuleSet = Map Color [Content]

-- Logic

reachable :: RuleSet -> Color -> Color -> Bool
reachable ruleSet color1 color2 = memoized_reachable color2
    where memoized_reachable = (makeMap reachable' (keys ruleSet) !)
          reachable' newColor
            | color1 == newColor = True
            | otherwise =
                let succColors = map fst (ruleSet ! newColor)
                 in if succColors == []
                       then False
                       else True `elem` map memoized_reachable succColors

contains :: RuleSet -> Color -> Int
contains ruleSet color =
    let myContent = ruleSet ! color
        countInners = (*) . contains ruleSet
     in if myContent == []
           then 1
           else (+1) $ sum $ map (uncurry countInners) myContent

-- Input parsing

bagParser :: Parsec String () Content
bagParser = flip (,) <$> fmap read (P.many1 P.digit)
                     <*  P.char ' '
                     <*> P.manyTill P.anyChar (P.try $ P.string " bag")
                     <*  P.optional (P.char 's')
                     <*  P.optional (P.string ", ")

ruleParser :: Parsec String () Rule
ruleParser = (,) <$> P.manyTill P.anyChar (P.try $ P.string " bags contain ")
                 <*  P.optional (P.string " no other bags.")
                 <*> P.option [] (P.manyTill bagParser $ P.char '.')

parseRuleSet :: String -> IO RuleSet
parseRuleSet = fmap (M.fromList . catMaybes) . sequence . map (runParserMaybeIO ruleParser) . lines

part1, part2 :: String -> IO Int
part1 content = do
    ruleSet <- parseRuleSet content
    let outerColors = nub $ keys ruleSet
    return $ subtract 1 $ length $ filter (\c -> reachable ruleSet "shiny gold" c) outerColors
part2 = fmap (subtract 1 . flip contains "shiny gold") . parseRuleSet
