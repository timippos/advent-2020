module Advent2020.Days.Day03 ( part1, part2 ) where

import Data.Matrix

-- Logic

traverseCarto :: Matrix Integer -> (Int, Int) -> (Int, Int) -> Integer
traverseCarto carto (r, c) (dr, dc)
  | r > nrows carto = 0
  | c > ncols carto = traverseCarto carto (r, c-ncols carto) (dr, dc)
  | otherwise       = traverseCarto carto (r+dr, c+dc) (dr, dc) + getElem r c carto

-- Input parsing

parseCarto :: String -> Matrix Integer
parseCarto content = 
    let input = lines content
        nr = length input ; nc = length $ head input
     in matrix nr nc (\(r, c) -> if input !! (r-1) !! (c-1) == '.' then 0 else 1)

part1, part2 :: String -> IO Integer
part1 content = return $ traverseCarto (parseCarto content) (1,1) (1,3)
part2 content = let carto = parseCarto content
                 in return $ product $ map (traverseCarto carto (1,1))
                                           [(1,1), (1,3), (1,5), (1,7), (2,1)]
