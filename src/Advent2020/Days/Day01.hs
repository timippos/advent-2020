module Advent2020.Days.Day01 ( part1, part2 ) where

-- Data structure

find2020 :: [Integer] -> (Integer, Integer)
find2020 xs = let combos = [(x, y) | x<-xs, y<-xs, x<y] in
                  head $ filter (\(x, y) -> x+y == 2020) combos

find2020' :: [Integer] -> (Integer, Integer, Integer)
find2020' xs = let combos = [(x, y, z) | x<-xs, y<-xs, z<-xs, x<y, y<z] in
                   head $ filter (\(x, y, z) -> x+y+z == 2020) combos

part1, part2 :: String -> IO Integer 
part1 = return . uncurry (*) . find2020 . map read . lines
part2 = return . (\(x, y, z) -> x*y*z) . find2020' . map read . lines
