module Advent2020.Days.Day02 ( part1, part2 ) where

import Control.Monad (join, filterM)
import Text.Parsec (Parsec)
import qualified Text.Parsec as P
import Advent2020.Text.Parsec

-- Data structure

type Validator = String -> IO Bool
type ValidatorMaker = Int -> Int -> Char -> Validator
type ValidatorParser = Parsec String () (Validator, String)

-- Logic

makeValidatorPart1 :: ValidatorMaker
makeValidatorPart1 min max chr str = do
    let matches = length $ filter (==chr) str
    return (matches >= min && matches <= max)

makeValidatorPart2 :: ValidatorMaker
makeValidatorPart2 min max chr str = do
    let firstMatch = str !! (min-1) == chr
    let secondMatch = str !! (max-1) == chr
    return $ (firstMatch && not secondMatch) || (secondMatch && not firstMatch)

validate :: ValidatorMaker -> String -> IO Bool
validate = fmap join . runParserIO (return False) . fmap (uncurry ($)) . makeLineParser
    
-- Input parsing

makeLineParser :: ValidatorMaker -> ValidatorParser
makeLineParser makeValidator = do
    min <- read <$> P.many1 P.digit
    P.string "-"
    max <- read <$> P.many1 P.digit
    P.string " "
    chr <- P.letter
    P.string ": "
    str <- P.many1 P.letter
    return (makeValidator min max chr, str)

part1, part2 :: String -> IO Int
part1 = fmap length . filterM (validate makeValidatorPart1) . lines
part2 = fmap length . filterM (validate makeValidatorPart2) . lines
