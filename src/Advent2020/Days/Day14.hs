{-# LANGUAGE TupleSections #-}

module Advent2020.Days.Day14 ( part1, part2 ) where

import Text.Parsec (Parsec)
import qualified Text.Parsec as P
import Advent2020.Text.Parsec (unsafeParse)
import Data.Map.Lazy (Map)
import qualified Data.Map.Lazy as M
import Advent2020.Data.Map.Lazy ((//))

-- Data structure

data Memory = Memory
    { mask :: String
    , cells :: Map Int Int
    } deriving (Show)

data Instruction = Mask String | Mem Int Int
    deriving (Eq, Show)

initMem = Memory { mask = "", cells = M.empty } :: Memory

-- Logic

sumMem :: Memory -> Int
sumMem (Memory _ cells) = sum $ M.elems cells

applyMaskPart1 :: String -> Int -> Int
applyMaskPart1 mask n
  | mask == "" = 0
  | otherwise  = applyOneBit mlast nlast + 2*(applyMaskPart1 minit ninit)
  where (minit, mlast) = (init mask, last mask)
        ninit = n `div` 2 ; nlast = n `mod` 2
        applyOneBit mbit b = case mbit of 'X' -> b ; '1' -> 1 ; '0' -> 0

runInstPart1 :: Memory -> Instruction -> Memory
runInstPart1 mem (Mask str) = Memory { mask = str, cells = cells mem }
runInstPart1 mem (Mem cell value) =
    let newvalue = applyMaskPart1 (mask mem) value
     in Memory { mask = mask mem, cells = (cells mem) // [(cell, newvalue)] }

applyMaskPart2 :: String -> Int -> String
applyMaskPart2 mask n 
  | mask == "" = ""
  | otherwise  = applyMaskPart2 minit ninit ++ applyOneBit mlast nlast
  where (minit, mlast) = (init mask, last mask)
        ninit = n `div` 2 ; nlast = n `mod` 2
        applyOneBit mbit b = case mbit of '0' -> show b ; x -> [x]

floatBit :: Char -> [Int]
floatBit b = case b of '0' -> [0] ; '1' -> [1] ; 'X' -> [0,1]

floatAddr :: String -> [Int]
floatAddr [abit] = floatBit abit
floatAddr addr = (+) <$> floatBit alast <*> map (*2) (floatAddr ainit)
    where (ainit, alast) = (init addr, last addr)

runInstPart2 :: Memory -> Instruction -> Memory
runInstPart2 mem (Mask str) = Memory { mask = str, cells = cells mem }
runInstPart2 mem (Mem cell value) =
    let newcells = floatAddr $ applyMaskPart2 (mask mem) cell
     in Memory { mask = mask mem
               , cells = (cells mem) // map (,value) newcells }

-- Input parsing

instParser :: Parsec String () Instruction
instParser = do
    instType <- P.choice $ map P.try [P.string "mask = ", P.string "mem["]
    case instType of
      "mask = " -> do
          mask <- P.count 36 (P.oneOf "X10")
          return $ Mask mask
      "mem[" -> do
          cell <- read <$> P.many1 P.digit
          P.string "] = "
          value <- read <$> P.many1 P.digit
          return $ Mem cell value

part1, part2 :: String -> IO Int
part1 = return . sumMem . foldl runInstPart1 initMem
               . map (unsafeParse instParser) . lines
part2 = return . sumMem . foldl runInstPart2 initMem
               . map (unsafeParse instParser) . lines
