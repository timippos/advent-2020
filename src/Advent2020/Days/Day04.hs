module Advent2020.Days.Day04 ( part1, part2 ) where

import Control.Monad (join)
import Data.Maybe (catMaybes)
import Data.List.Split (splitOn)
import Data.Either.Extra (eitherToMaybe)
import Safe (readMay)
import Text.Parsec (parse, Parsec)
import qualified Text.Parsec as P 
import Advent2020.Text.Parsec

-- Input parsing

parseYear :: Int -> Int -> String -> Bool
parseYear min max value =
    case readMay value of
      Just valueInt -> valueInt >= min && valueInt <= max
      Nothing -> False

heightParser :: String -> Parsec String () (Maybe String)
heightParser key = do
    num <- P.many1 P.digit
    unit <- P.count 2 P.letter
    let minCm = 150 ; maxCm = 193 ; minIn = 59 ; maxIn = 76
    let numInt = read num
    P.eof
    case unit of
      "cm" -> if numInt >= minCm && numInt <= maxCm
                 then return $ Just key
                 else return Nothing
      "in" -> if numInt >= minIn && numInt <= maxIn
                 then return $ Just key
                 else return Nothing
      _    -> return Nothing

hexColorParser :: String -> Parsec String () String
hexColorParser key = do
    P.char '#'
    P.count 6 P.hexDigit
    P.eof
    return key

pidParser :: String -> Parsec String () String
pidParser key = P.count 9 P.digit >> P.eof >> return key

keyParserPart1 :: Parsec String () (Maybe String)
keyParserPart1 = do
    key <- P.many1 P.letter
    P.char ':'
    value <- P.many1 (P.noneOf " \n")
    P.try $ P.optional $ P.oneOf " \n"
    return $ Just key

keyParserPart2 :: Parsec String () (Maybe String)
keyParserPart2 = do
    key <- P.many1 P.letter
    P.char ':'
    value <- P.many1 (P.noneOf " \n")
    P.try $ P.optional $ P.oneOf " \n"
    return $ case key of
               "byr" -> if parseYear 1920 2002 value then Just key else Nothing
               "iyr" -> if parseYear 2010 2020 value then Just key else Nothing
               "eyr" -> if parseYear 2020 2030 value then Just key else Nothing
               "hgt" -> join $ eitherToMaybe $ parse (heightParser key) value value
               "hcl" -> eitherToMaybe $ parse (hexColorParser key) value value
               "ecl" -> if value `elem` ["amb", "blu", "brn", "gry", "grn", "hzl", "oth"]
                           then Just key else Nothing
               "pid" -> eitherToMaybe $ parse (pidParser key) value value
               _     -> Nothing

entryParser :: Parsec String () (Maybe String) -> Parsec String () Int
entryParser keyParser = do
    fields <- P.many1 keyParser
    let keys = catMaybes fields
    let result = foldr (&&) True
               $ map (flip elem keys)
               $ ["byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"]
    if result then return 1 else return 0

-- Input parsing

part1 = fmap sum . sequence
                 . map (runParserIO 0 $ entryParser keyParserPart1)
                 . splitOn "\n\n"
part2 = fmap sum . sequence
                 . map (runParserIO 0 $ entryParser keyParserPart2)
                 . splitOn "\n\n"
