module Advent2020.Days.Day10 ( part1, part2 ) where

import Control.Monad (liftM2)
import Data.List (sort)
import Data.Map.Lazy ( (!) )
import Advent2020.Data.List (countElem)
import Advent2020.Data.Map.Lazy (makeMap)

-- Logic

listDiff :: [Int] -> [Int]
listDiff = zipWith subtract =<< (0:)

calcRating :: [Int] -> Integer
calcRating = toInteger . liftM2 (*) (countElem 1) ((+1) . countElem 3) . listDiff . sort

combAdapters  :: [Int] -> Integer
combAdapters unsortedAs = memoized_combAdapters 0
    where as = 0 : sort unsortedAs
          memoized_combAdapters = (makeMap combAdapters' as !)
          combAdapters' x
            | x >= maximum as = 1
            | otherwise = sum $ map memoized_combAdapters $ filter (\y -> y>x && y-x<=3) as

-- Input parsing

parseContent :: String -> [Int]
parseContent = map read . lines

part1, part2 :: String -> IO Integer
part1 = return . calcRating . parseContent
part2 = return . combAdapters . parseContent
