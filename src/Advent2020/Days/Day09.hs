module Advent2020.Days.Day09 ( part1, part2 ) where

import Control.Monad (join, liftM2)
import Data.Maybe (catMaybes)
import Safe (headMay)

import Advent2020.Data.List (partialSums, indexed, takeBetween)

-- Logic

findInvalid :: Int -> [Integer] -> Maybe Integer
findInvalid hdr ns = if null indexInvalids then Nothing else Just $ ns !! head indexInvalids
    where indexInvalids = filter (not . validAt ns) [hdr..length ns-1]
          validAt ns i | i < hdr = True
                       | otherwise = let precs = take hdr $ drop (i-hdr) ns
                                      in True `elem` map (\n -> (ns!!i)-n `elem` precs) precs

-- Find a continuous subarray that sums to k
-- returns the index of the beginning and the end of the range
findSubarrayWithSum :: [Integer] -> Integer -> Maybe (Int, Int)
findSubarrayWithSum ns k = join $ headMay $ filter (not . null) $ map match [0..length ns-1]
    where psums = partialSums ns
          match i = headMay $ catMaybes
                            $ map (\(j, n) -> if (psums!!i)-n == k then Just (j+1, i) else Nothing)
                            $ indexed $ take (i-1) psums

-- Input parsing

parseCode :: String -> [Integer]
parseCode = map read . lines

part1, part2 :: String -> IO (Maybe Integer)
part1 = return . findInvalid 25 . parseCode
part2 content = return $ join $ solver code <$> findInvalid 25 code
    where code = parseCode content
          solver code key = fmap (\(i, j) -> liftM2 (+) minimum maximum $ takeBetween i j code)
                                 (findSubarrayWithSum code key)
