module Advent2020.Days.Day13 ( part1, part2 ) where

import Data.List (sortOn)
import Data.Tuple.Extra (second)
import Data.List.Extra (split, minimumOn)

-- Logic

getMinAfter :: [Integer] -> Integer -> Integer
getMinAfter buses lb = minBus * waitTime
    where minBus = minimumOn ((-) <*> (lb `mod`)) buses
          waitTime = minBus - (lb `mod` minBus)

reduce :: [(Integer, Integer)] -> [(Integer, Integer)]
reduce = reverse . sortOn snd . map (\(a, n) -> (a `mod` n, n))

-- Get Bézout's coefficients using the extended Euclidean algorithm
-- bezout a b = (s, t) where as + bt = gcd a b
bezout :: Integer -> Integer -> (Integer, Integer)
bezout a b = eea a 1 0 b 0 1
    where eea r s t r' s' t'
            | r' == 0 = (s, t)
            | otherwise = let q = r `div` r'
                           in eea r' s' t' (r-q*r') (s-q*s') (t-q*t')

solveCRT :: [(Integer, Integer)] -> Integer
solveCRT [(a, n)] = a
solveCRT ((a1, n1):(a2, n2):rest) = solveCRT ((a', n'):rest)
    where (s, t) = bezout n1 n2
          (a', n') = ((t*a1*n2 + s*a2*n1) `mod` (n1*n2), n1*n2)

-- Input parsing

parseContent :: String -> (Integer, [(Integer, Integer)])
parseContent content = (read timestamp, services)
    where (timestamp:serviceList:_) = lines content
          services = map (second read) $ filter ((/="x") . snd)
                                       $ zip [0..]
                                       $ split (==',') serviceList

part1, part2 :: String -> IO Integer
part1 content = return $ getMinAfter buses lb
    where (lb, services) = parseContent content
          buses = snd $ unzip services
part2 = return . solveCRT . reduce . map (\(a, n) -> (n-a, n))
               . snd . parseContent
