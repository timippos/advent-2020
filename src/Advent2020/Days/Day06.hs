module Advent2020.Days.Day06 ( part1, part2 ) where

import Data.List (nub)
import Data.List.Split (splitOn)

-- Logic

groupCountAny :: String -> Int
groupCountAny = length . nub . filter (/='\n')

groupCountAll :: String -> Int
groupCountAll = length . flip filter ['a'..'z'] . flip (all . elem) . lines

-- Input parsing

part1, part2 :: String -> IO Int
part1 = return . sum . map groupCountAny . splitOn "\n\n"
part2 = return . sum . map groupCountAll . splitOn "\n\n"
