module Advent2020.Days.Day12 ( part1, part2 ) where

import Text.Parsec (Parsec)
import qualified Text.Parsec as P 
import Advent2020.Text.Parsec

-- Data structure

data Heading = North | East | South | West
    deriving (Eq, Show)

type Coord = (Integer, Integer)
data Position = Position
    { heading :: Heading
    , coord :: Coord
    } deriving (Show)

origin = (0, 0) :: Coord

data Position2 = Position2
    { ship :: Coord
    , waypoint :: Coord
    } deriving (Show)

data Instruction = Move Heading Integer
                 | TurnLeft Integer | TurnRight Integer
                 | Forward Integer
                 deriving (Eq, Show)

-- Logic 

manhattanDist :: Coord -> Coord -> Integer
manhattanDist (x1, y1) (x2, y2) = abs (x1-x2) + abs (y1-y2)

degreeToHeading :: Integer -> Heading
degreeToHeading deg = case deg `mod` 360 of
                        0 -> East ; 90 -> North ; 180 -> West ; 270 -> South
                        x -> error ("not a due heading: " ++ show x)

headingToDegree :: Heading -> Integer
headingToDegree h = case h of
                      East -> 0 ; North -> 90 ; West -> 180 ; South -> 270

go :: Coord -> Heading -> Integer -> Coord
go (x, y) h d = case h of
                   East ->  (x+d, y)
                   West ->  (x-d, y)
                   North -> (x, y+d)
                   South -> (x, y-d)

turn :: Heading -> Integer -> Heading
turn h deg = degreeToHeading (deg + headingToDegree h)

applyInstPart1 :: Position -> Instruction -> Position
applyInstPart1 p i =
    let (Position h (x, y)) = p
     in case i of
          Move h' d   -> Position h (go (x, y) h' d)
          TurnLeft d  -> Position (turn h d) (x, y)
          TurnRight d -> Position (turn h $ negate d) (x, y)
          Forward d   -> Position h (go (x, y) h d)

rotate :: Coord -> Integer -> Coord
rotate (x, y) deg = (x*cosInt deg - y*sinInt deg,
                     x*sinInt deg + y*cosInt deg)
    where cosInt deg = case deg `mod` 360 of
                         0 -> 1 ; 90 -> 0 ; 180 -> -1 ; 270 -> 0
          sinInt deg = case deg `mod` 360 of
                         0 -> 0 ; 90 -> 1 ; 180 -> 0 ; 270 -> -1

applyInstPart2 :: Position2 -> Instruction -> Position2
applyInstPart2 (Position2 s wpt) i =
    let (x, y) = s ; (dx, dy) = wpt
     in case i of
          Move h d    -> Position2 s (go wpt h d)
          TurnLeft d  -> Position2 s (rotate wpt d) 
          TurnRight d -> Position2 s (rotate wpt $ negate d)
          Forward n   -> Position2 (x+n*dx, y+n*dy) wpt

-- Input parsing

instParser :: Parsec String () Instruction
instParser = do
    instType <- P.letter
    param <- read <$> P.many1 P.digit
    return $ case instType of
             'N' -> Move North param ; 'S' -> Move South param
             'E' -> Move East param  ; 'W' -> Move West param
             'L' -> TurnLeft param   ; 'R' -> TurnRight param
             'F' -> Forward param

part1, part2 :: String -> IO Integer
part1 = return . manhattanDist origin . coord
               . foldl applyInstPart1 (Position East origin)
               . map (unsafeParse instParser) . lines
part2 = return . manhattanDist origin . ship
               . foldl applyInstPart2 (Position2 origin (10, 1))
               . map (unsafeParse instParser) . lines
