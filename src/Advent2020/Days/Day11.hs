module Advent2020.Days.Day11 ( part1, part2 ) where

import Control.Monad (join, liftM2)
import Control.Arrow ((***))
import Data.Maybe (catMaybes)
import Safe (headMay)
import Data.Matrix

-- Data structure

data Block = Empty | Occupied | PermEmpty | PermOccupied | Floor deriving (Eq)
instance (Show Block) where
    show Empty = "L" ; show Occupied = "#" ; show Floor = "."
    show PermEmpty = "O" ; show PermOccupied = "*"

occupiedTypes = [Occupied, PermOccupied] :: [Block]
invariantTypes = [PermEmpty, PermOccupied, Floor] :: [Block]

toBlock :: Char -> Block
toBlock 'L' = Empty ; toBlock '#' = Occupied ; toBlock '.' = Floor

type SeatMap = Matrix Block

-- Logic

isOutOfBounds :: SeatMap -> (Int, Int) -> Bool
isOutOfBounds sm (x, y) = x < 1 || y < 1 || x > nrows sm || y > ncols sm

getAdjacent :: SeatMap -> (Int, Int) -> [(Int, Int)]
getAdjacent sm (x, y)
  | isOutOfBounds sm (x, y) = []
  | otherwise = liftM2 (,) (getValids x $ nrows sm) (getValids y $ ncols sm) where
      getValids n max = filter (\m -> m>0 && m<=max) [n-1, n, n+1]

getFirstSeatVisible :: SeatMap -> (Int, Int) -> (Int, Int) -> Maybe Block
getFirstSeatVisible sm (x, y) (dx, dy) =
    headMay $ filter (/=Floor) $ map (sm !)
            $ takeWhile (not . isOutOfBounds sm)
            $ drop 1 $ iterate ((+dx) *** (+dy)) (x, y)

type CountFunction = SeatMap -> (Int, Int) -> Int
type EvolveFunction = SeatMap -> (Int, Int) -> Block -> Block

countAdjacent :: [Block] -> CountFunction
countAdjacent blks sm (x, y) =
    length $ filter (\coord -> sm ! coord `elem` blks) $ getAdjacent sm (x, y)

countVisible :: [Block] -> CountFunction
countVisible blks sm (x, y) =
    length $ filter (`elem` blks) $ catMaybes
           $ map (getFirstSeatVisible sm (x, y)) directions
    where directions = filter (/=(0,0)) $ join (liftM2 (,)) [-1, 0, 1]

evolveBlock :: CountFunction -> EvolveFunction
evolveBlock countBad sm coord blk
  | blk `elem` invariantTypes = blk
  | otherwise = let nBad = countBad sm coord
                 in case blk of 
                      Empty    -> if nBad == 0 then Occupied else PermEmpty
                      Occupied -> if nBad >= 5 then Empty else PermOccupied

evolveOneStep :: EvolveFunction -> SeatMap -> SeatMap
evolveOneStep efn sm = mapPos (efn sm) sm

evolveUntilStable :: EvolveFunction -> SeatMap -> SeatMap
evolveUntilStable = liftM2 (<*>) evolveUntilStable' evolveOneStep
    where evolveUntilStable' efn prev cur 
            | prev == cur = cur
            | otherwise   = evolveUntilStable' efn cur (evolveOneStep efn cur)

countBlkTypes :: [Block] -> SeatMap -> Int
countBlkTypes blks sm = length $ filter (`elem` blks) $ toList sm

-- Input parsing

parseSeatMap :: String -> SeatMap
parseSeatMap = fromLists . map (map toBlock) . lines

part1, part2 :: String -> IO Int
part1 = return . countBlkTypes occupiedTypes
               . evolveUntilStable (evolveBlock $ countAdjacent occupiedTypes)
               . parseSeatMap
part2 = return . countBlkTypes occupiedTypes
               . evolveUntilStable (evolveBlock $ countVisible occupiedTypes)
               . parseSeatMap
