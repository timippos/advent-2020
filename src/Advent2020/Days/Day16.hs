module Advent2020.Days.Day16 ( part1, part2 ) where

import Control.Monad
import Control.Concurrent
import Data.Maybe
import Data.List
import Data.Function
import Safe
import Data.Map.Lazy (Map, (!))
import qualified Data.Map.Lazy as M
import Advent2020.Data.Map.Lazy (makeMap)
import Data.Set (Set)
import qualified Data.Set as S
import Text.Parsec (Parsec)
import qualified Text.Parsec as P
import Data.List.Split (splitOn)
import Advent2020.Text.Parsec (runParserErrorIO)

-- Data structure

data Rule = Rule
    { ruleName :: String
    , validRanges :: [(Int, Int)]
    } deriving (Eq, Ord)
instance (Show Rule) where
    show (Rule name _) = "Rule " ++ show name

type Ticket = [Int]

runRule :: Rule -> Int -> Bool
runRule (Rule _ ranges) n = any (\(x, y) -> x<=n && n<=y) ranges

-- Logic

getErrors :: [Rule] -> Ticket -> [Int]
getErrors rules tik = filter (isInvalid rules) tik
    where isInvalid rules n = all (not . flip runRule n) rules

getValidRules :: [Int] -> [Rule] -> [Rule]
getValidRules field = filter (\rule -> all (runRule rule) field)

permRules :: [Rule] -> [[Rule]] -> [[Rule]]
permRules rules validRules = filter (\perm -> isAllowed perm) $ permutations rules
    where isAllowed perm = all (\i -> (perm !! i) `elem` (validRules !! i)) [0..(length perm)-1]

-- Horribly slow :(
searchGraph :: [(Int, Rule)] -> Int -> [(Int, Rule)] -> Maybe [(Int, Rule)]
searchGraph graph n path
  | n == -1 = Just path
  | otherwise = if null thisRules
                   then Nothing
                   else headMay $ catMaybes $ map (\node -> searchGraph graph (n-1) (node:path)) thisRules
                       where isVisited rule = not $ null $ filter (\(_, r) -> r == rule) path
                             thisRules = filter (not . isVisited . snd) $ filter ((==n) . fst) graph

decodeFields :: [Rule] -> [Ticket] -> IO (Maybe [String])
decodeFields rules tix =
    let validRules = map (\n -> filter (\rule -> all (runRule rule) n) rules) (transpose tix)
        graph = concatMap (\(n, rules) -> zip (repeat n) rules) $ zip [0..] validRules
     in do
         let results = searchGraph graph (length rules-1) []
         print $ results
         return $ fmap (map (\(n, Rule name _) -> name)) results

-- Input parsing

ruleParser :: Parsec String () Rule
ruleParser = do
    name <- P.manyTill (P.choice [P.letter, P.char ' ']) (P.string ": ")
    ranges <- P.many1 ( (,) <$> fmap read (P.many1 P.digit)
                            <*  P.char '-'
                            <*> fmap read (P.many1 P.digit)
                            <*  P.optional (P.try $ P.string " or ") )
    return Rule { ruleName = name, validRanges = ranges }

parseFile :: String -> IO ([Rule], Ticket, [Ticket])
parseFile file = let (strRules:strMyTik:strNbTix:_) = splitOn "\n\n" file
                     rules = sequence $ map (runParserErrorIO ruleParser) (lines strRules)
                     myTik = map read $ splitOn "," $ head $ tail $ lines strMyTik
                     nbTix = map (map read . splitOn ",") $ tail $ lines strNbTix
                  in (,,) <$> rules <*> return myTik <*> return nbTix

part1 content = do
    (rules, myTik, nbTix) <- parseFile content
    return $ sum $ concatMap (getErrors rules) nbTix

part2 content = do
    (rules, myTik, nbTix) <- parseFile content
    let validTix = filter (null . getErrors rules) nbTix
    fieldNames <- decodeFields rules validTix
    let myTikParsed = flip zip myTik <$> fieldNames
    let myTikGoodFields = filter (\(name, n) -> "departure" `isPrefixOf` name) <$> myTikParsed
    return $ fmap (product . map snd) myTikGoodFields
