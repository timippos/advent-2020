module Advent2020.Text.Parsec
    ( runParserIO
    , runParserMaybeIO
    , runParserErrorIO
    , unsafeParse
    ) where

import Text.Parsec
import Control.Monad (join)
import Data.Either.Unwrap (fromRight)

unsafeParse :: Parsec String () a -> String -> a
unsafeParse p = fromRight . join (parse p)

runParserIO :: a -> Parsec String () a -> String -> IO a
runParserIO defval parser input =
    let result = parse parser input input
     in case result of
          Left x -> putStrLn (show x) >> return defval
          Right x -> return x

runParserErrorIO :: Parsec String () a -> String -> IO a
runParserErrorIO = runParserIO undefined

runParserMaybeIO :: Parsec String () a -> String -> IO (Maybe a)
runParserMaybeIO = runParserIO Nothing . fmap Just
