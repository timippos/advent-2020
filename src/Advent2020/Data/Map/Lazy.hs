module Advent2020.Data.Map.Lazy
    ( makeMap
    , (//)
    ) where

import Data.Map.Lazy (Map, (!))
import qualified Data.Map.Lazy as M

makeMap :: Ord k => (k -> a) -> [k] -> Map k a
makeMap f xs = M.fromList $ zip xs (map f xs) 

(//) :: Ord k => Map k a -> [(k, a)] -> Map k a
(//) mp list = foldl (\m (key, value) -> M.insert key value m) mp list
