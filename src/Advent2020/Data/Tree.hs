module Advent2020.Data.Tree
    ( filterF
    , getTreePaths
    , getPaths
    ) where

import Data.Tree

filterF :: (a -> Bool) -> Forest a -> Forest a
filterF _ [] = []
filterF f ((Node x xs):rest) =
    if f x then ((Node x (filterF f xs)):filterF f rest)
           else filterF f rest

getPaths :: Forest a -> [[a]]
getPaths = concatMap getTreePaths

getTreePaths :: Tree a -> [[a]]
getTreePaths = foldTree (\x xs -> if null xs then [[x]] else concat $ map (map (\path -> (x:path))) xs)

t = [Node 1 [Node 2 [], Node 3 []]] :: Forest Int
tt = head t
