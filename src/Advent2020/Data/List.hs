module Advent2020.Data.List
    ( combination
    , combinationNoDiag
    , combination3
    , combination3NoDiag
    , partialSums
    , indexed
    , takeBetween
    , countElem
    ) where

import Control.Monad
import Data.List

-- Combinations of a list with itself (order does not matter)
-- The function is from this StackExchange answer:
--     http://stackoverflow.com/a/25900462 
combination :: [a] -> [(a,a)]
combination = join . (zipWith (zip . repeat) `ap` tails)

combinationNoDiag :: [a] -> [(a,a)]
combinationNoDiag = join . (zipWith (zip . repeat) `ap` (tail . tails))

combination3 :: [a] -> [(a,a,a)]
combination3 xs = [(a,b,c) | a <- xs, b <- xs, c <- xs]

combination3NoDiag :: (Ord a) => [a] -> [(a,a,a)]
combination3NoDiag xs = [(a,b,c) | a <- xs, b <- xs, c <- xs, a <= b, b <= c]

-- For a sequence (a_i)_{0<=i<=n}, we generate partial sums (\sum_0^i a_j)_{0<=i<=n} 
partialSums :: [Integer] -> [Integer]
partialSums = drop 1 . scanl (+) 0

indexed :: [a] -> [(Int, a)]
indexed xs = zip [0..length xs-1] xs

takeBetween :: Int -> Int -> [Integer] -> [Integer]
takeBetween i j xs = drop i $ take (j+1) xs

countElem :: (Eq a) => a -> [a] -> Int
countElem x = length . filter (==x)
