module Advent2020.Data.Graph
    ( Graph (..)
    , UnweightedGraph
    , Association2
    , fromGraph
    , toGraph
    , toSymGraph
    , toUnweightedGraph
    , toUnweightedSymGraph
    , graphToUnweighted
    , lookupGraph
    , isSymmetric
    , edgeSymmetric
    ) where

import Control.Monad
import Data.Maybe
import Data.List
import Data.Matrix
import qualified Data.Matrix as Matrix

-- Main data types

data Graph lab val = Graph
    { vertices :: [lab]
    , weights :: Matrix (Maybe val)
    } 
instance (Show lab, Show val) => Show (Graph lab val) where
    show (Graph labs wts) =
        "Graph: labels = " ++ show labs ++ ", weights =\n" ++ show wts 

type Association2 lab val = ((lab, lab), val)

-- Derived data types

type UnweightedGraph lab = Graph lab ()

-- Logic
-- Building graphs

getVertices :: Eq lab => [Association2 lab val] -> [lab]
getVertices = nub . concatMap (\(x,y) -> [x,y]) . fst . unzip

toGraph :: Eq lab => [Association2 lab val] -> Graph lab val
toGraph g = let vertices = getVertices g
                size = length vertices
                lookupAssociation2 x y = lookup (xLabel, yLabel) g
                    where xLabel = vertices !! (x-1)
                          yLabel = vertices !! (y-1)
                wts = matrix size size (uncurry lookupAssociation2)
             in Graph vertices wts

toSymGraph :: Eq lab => [Association2 lab val] -> Graph lab val
toSymGraph g = toGraph $ concatMap (\((x,y),w) -> [((x,y),w),((y,x),w)]) g

toUnweightedGraph :: Eq lab => [(lab,lab)] -> UnweightedGraph lab
toUnweightedGraph g = toGraph $ map (\edge -> (edge, ())) g

toUnweightedSymGraph :: Eq lab => [(lab,lab)] -> UnweightedGraph lab
toUnweightedSymGraph g = toUnweightedGraph $ nub $ concatMap (\(x,y) -> [(x,y),(y,x)]) g

graphToUnweighted :: Graph lab val -> UnweightedGraph lab
graphToUnweighted (Graph vts wts) = (Graph vts deweighted)
    where deweighted = mapPos (\_ w -> fmap (\_ -> ()) w) wts

fromGraph :: Graph lab val -> [Association2 lab val]
fromGraph (Graph vts wts) =
    let allEdges = join (liftM2 (,)) [0..length vts-1]
     in catMaybes $
          map (\(x, y) ->
            case wts ! (x+1, y+1) of
              Nothing -> Nothing
              Just w  -> Just ((vts !! x, vts !! y), w)) allEdges

-- Accessing graphs

lookupGraph :: Eq lab => (lab, lab) -> Graph lab val -> Maybe val
lookupGraph (v1, v2) (Graph vts wts) =
    let may_i1 = findIndex (==v1) vts ; may_i2 = findIndex (==v2) vts
     in case (may_i1, may_i2) of
          (Just i1, Just i2) -> wts ! (i1+1, i2+1)
          _ -> Nothing

isSymmetric :: (Eq val) => Graph lab val -> Bool
isSymmetric (Graph _ wts) = wts == Matrix.transpose wts

edgeSymmetric :: (Eq lab) => (lab, lab) -> Graph lab val -> Bool
edgeSymmetric (v1, v2) graph = isJust w1 || isJust w2
    where (w1, w2) = (lookupGraph (v1, v2) graph, lookupGraph (v2, v1) graph) 

-- A small test case 
t = [ (('A', 'B'), 12), (('A', 'G'), 5)
    , (('B', 'A'), 12), (('B', 'D'), 3)
    , (('C', 'D'), 1)
    , (('D', 'B'), 3), (('D', 'C'), 1), (('D', 'E'), 6)
    , (('E', 'D'), 6), (('E', 'F'), 4)
    , (('F', 'E'), 4), (('F', 'G'), 7), (('F', 'H'), 3)
    , (('G', 'F'), 7), (('G', 'H'), 2), (('G', 'A'), 5)
    , (('H', 'G'), 2), (('H', 'F'), 3)
    ] :: [Association2 Char Int]
g = toGraph t :: Graph Char Int
