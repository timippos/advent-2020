module Advent2020.Data.Graph.Algorithms.HopcroftKarp
    ( maxMatchingBipartite
    , relabel
    ) where

import Data.Maybe
import Safe
import Data.List
import Data.Either
import Data.Map.Strict (Map, (!))
import qualified Data.Map.Strict as Map
import Data.Tree
import Advent2020.Data.Graph
import Advent2020.Data.Graph.Bipartite
import Advent2020.Data.Tree

C.context (C.baseCtx <> C.vecCtx)

type UIBGraph = UnweightedBipartiteGraph Int Int

forceLeft :: Either a b -> a
forceRight :: Either a b -> b
forceLeft = fromLeft undefined
forceRight = fromRight undefined

relabel :: (Eq lab1, Eq lab2, Ord lab1, Ord lab2) =>
    UnweightedBipartiteGraph lab1 lab2 -> (UIBGraph, Map lab1 Int, Map lab2 Int)
relabel graph = (uibGraph, dictLeft, dictRight)
    where leftVts = getLeftVertices graph
          rightVts = getRightVertices graph
          dictLeft = Map.fromList $ zip leftVts [1..]
          dictRight = Map.fromList $ zip rightVts [1..]
          uibGraph = toUnweightedBipartiteGraph $
              map (\(k1, k2) -> (dictLeft ! k1, dictRight ! k2)) $
                  bipartiteEdges graph

freenodes :: UIBGraph -> [(Int, Int)] -> ([Int], [Int])
freenodes graph matching = (freeLefts, freeRights)
    where leftVts = getLeftVertices graph
          rightVts = getRightVertices graph
          freeLefts  = filter (\v -> all (\(v1, v2) -> v1 /= v) matching) $ leftVts
          freeRights = filter (\v -> all (\(v1, v2) -> v2 /= v) matching) $ rightVts

graphToLayers :: UIBGraph -> [(Int, Int)] -> Maybe ([[Int]], [[Int]])
graphToLayers graph matching = extendR freeLefts [] ([freeLefts], [])
    where leftVts = getLeftVertices graph
          rightVts = getRightVertices graph
          edges = bipartiteEdges graph 
          freeEdges = edges \\ matching 
          (freeLefts, freeRights) = freenodes graph matching

          getNeighborsR lefts = filter (\rvt ->
              any (\lvt -> (lvt, rvt) `elem` freeEdges) lefts) rightVts
          getNeighborsL rights = filter (\lvt ->
              any (\rvt -> (lvt, rvt) `elem` matching) rights) leftVts

          extendR visitedLefts visitedRights (lastLefts, lastRights) =
              let lastLayer = last lastLefts
                  nextLayer = getNeighborsR lastLayer
               in case nextLayer `intersect` freeRights of
                    [] -> if null nextLayer
                             then Nothing
                             else extendL visitedLefts (visitedRights++nextLayer)
                                          (lastLefts, lastRights++[nextLayer])
                    frees -> Just (lastLefts, lastRights++[frees])

          extendL visitedLefts visitedRights (lastLefts, lastRights) =
              let lastLayer = last lastRights
                  nextLayer = getNeighborsL lastLayer
               in case nextLayer `intersect` freeLefts of
                    [] -> if null nextLayer
                             then Nothing
                             else extendR (visitedLefts++nextLayer) visitedRights
                                          (lastLefts++[nextLayer], lastRights)
                    frees -> Just (lastLefts++[frees], lastRights)

findAugmenting :: UIBGraph -> [(Int, Int)] -> [[(Int, Int)]]
findAugmenting graph matching = 
    let layers = graphToLayers graph matching
        leftVts = getLeftVertices graph
        rightVts = getRightVertices graph
        edges = bipartiteEdges graph 
        freeEdges = edges \\ matching 
        (freeLefts, freeRights) = freenodes graph matching

        getNeighborsR lefts = filter (\rvt ->
            any (\lvt -> (lvt, rvt) `elem` freeEdges) lefts) rightVts
        getNeighborsL rights = filter (\lvt ->
            any (\rvt -> (lvt, rvt) `elem` matching) rights) leftVts

        makeSearchTree = makeSearchTreeR
        makeSearchTreeL ls rs 
          | null rs = []
          | otherwise = map (\r -> Node r $ makeSearchTreeR ls (tail rs)) $ head rs
        makeSearchTreeR ls rs 
          | null ls = []
          | otherwise = map (\l -> Node l $ makeSearchTreeL (tail ls) rs) $ head ls

        pruneTree ns tree = filterF (not . (`elem` ns)) tree 
        testPath p 
          | length p <= 1         = True
          | length p `mod` 2 == 0 =
              let (p1:p2:rest) = p
               in (p2 `elem` getNeighborsR [p1]) && testPath (p2:rest)
          | length p `mod` 2 == 1 =
              let (p1:p2:rest) = p
               in (p2 `elem` getNeighborsL [p1]) && testPath (p2:rest)

        validAugmenting tree =
            let match = headMay $ filter testPath
                                $ getPaths tree
             in case match of
                  Nothing -> []
                  Just p -> (p : (validAugmenting $ pruneTree p tree))

        translatePath p
          | length p <= 1         = []
          | length p `mod` 2 == 0 =
              let (p1:p2:rest) = p
               in ((p1, p2):(translatePath (p2:rest)))
          | length p `mod` 2 == 1 =
              let (p1:p2:rest) = p
               in ((p2, p1):(translatePath (p2:rest)))

     in case layers of
          Nothing -> []
          Just (layersL, layersR) -> 
              filter (not . null) $ map translatePath $ validAugmenting
                                  $ makeSearchTree layersL layersR

applyAugment :: UIBGraph -> [(Int, Int)] -> [(Int, Int)] -> [(Int, Int)]
applyAugment graph matching augment =
    let edges = bipartiteEdges graph 
        freeEdges = edges \\ matching
     in nub $ (matching \\ augment) ++ (freeEdges `intersect` augment)

updateMatching :: UIBGraph -> [(Int, Int)] -> [(Int, Int)]
updateMatching graph matching =
    let augments = findAugmenting graph matching :: [[(Int, Int)]]
     in if null augments
           then matching
           else updateMatching graph (foldl (applyAugment graph) matching augments)

maxMatchingBipartite :: UIBGraph -> [(Int, Int)]
maxMatchingBipartite graph = updateMatching graph []

-- Test case
t = [ (1,'A'),(1,'B')
    , (2,'A'),(2,'C'),(2,'D'),(2,'E')
    , (3,'A'),(3,'C')
    , (4,'B'),(4,'C'),(4,'D'),(4,'E')
    , (5,'C')
    ] :: [(Int, Char)]
t' = [ (1,1),(2,1),(2,2),(2,3),(3,1),(3,4),(4,2),(4,5),(5,3),(5,4) ]
g = toUnweightedBipartiteGraph t :: UnweightedBipartiteGraph Int Char
g' = toUnweightedBipartiteGraph t' :: UIBGraph
