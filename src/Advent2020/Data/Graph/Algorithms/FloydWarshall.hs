module Advent2020.Data.Graph.Algorithms.FloydWarshall
    ( shortest
    ) where

import Data.Maybe
import Data.Matrix
import Data.List
import qualified Data.Map.Lazy as Map
import Advent2020.Data.Graph
import Advent2020.Data.List
import Advent2020.Data.Map.Lazy (makeMap)

-- Uses Floyd-Warshall algorithm to find the shortest path between two vertices
shortest :: (Eq lab, Ord val, Num val) => Graph lab val -> (lab, lab) -> Maybe val
shortest (Graph vts wts) (v1, v2) = memoized_d (toIndex v1, toIndex v2, numVts-1)
    where toIndex v = fromJust $ findIndex (==v) vts
          numVts = length vts
          memoized_d = (makeMap plain_d (combination3 [0..numVts-1]) Map.!)
          plain_d (i, j, k)
            | k == 0 = wts ! (i+1, j+1)
            | otherwise =
                case ( memoized_d (i, j, k-1)
                     , memoized_d (i, k, k-1)
                     , memoized_d (k, j, k-1) ) of
                  (Just curBest, Just newBest1, Just newBest2) ->
                      Just (minimum [curBest, newBest1 + newBest2])
                  (Just curBest, _, _) -> Just curBest
                  (Nothing, Just newBest1, Just newBest2) -> Just (newBest1 + newBest2)
                  _ -> Nothing
