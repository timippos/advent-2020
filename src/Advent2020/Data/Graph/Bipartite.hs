module Advent2020.Data.Graph.Bipartite
    ( BipartiteGraph
    , BipartiteAssociation2
    , UnweightedBipartiteGraph
    , toBipartiteGraph
    , toUnweightedBipartiteGraph
    , getLeftVertices
    , getRightVertices
    , bipartiteEdges
    ) where

import Advent2020.Data.Graph
import Data.Either
import Data.Matrix

-- Data types

type BipartiteAssociation2 lab1 lab2 val = ((lab1, lab2), val)
type BipartiteGraph lab1 lab2 val = Graph (Either lab1 lab2) val
type UnweightedBipartiteGraph lab1 lab2 = BipartiteGraph lab1 lab2 ()

-- Building bipartite graphs

toBipartiteGraph :: (Eq lab1, Eq lab2) =>
    [BipartiteAssociation2 lab1 lab2 val] -> BipartiteGraph lab1 lab2 val
toBipartiteGraph g = toGraph $ map (\((x,y),w) -> ((Left x, Right y), w)) g

toUnweightedBipartiteGraph :: (Eq lab1, Eq lab2) =>
    [(lab1,lab2)] -> UnweightedBipartiteGraph lab1 lab2
toUnweightedBipartiteGraph g = toBipartiteGraph $ map (\edge -> (edge, ())) g

-- Bipartite graph-specific functions

getLeftVertices :: BipartiteGraph lab1 lab2 val -> [lab1]
getLeftVertices (Graph vts wts) = map (fromLeft undefined) $ filter isLeft vts

getRightVertices :: BipartiteGraph lab1 lab2 val -> [lab2]
getRightVertices (Graph vts wts) = map (fromRight undefined) $ filter isRight vts

bipartiteEdges :: BipartiteGraph lab1 lab2 val -> [(lab1, lab2)]
bipartiteEdges graph = map (\((Left v1, Right v2),w) -> (v1, v2)) $ fromGraph graph 
